package com.example.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ActionMenuView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.fragment.app.ListFragment;

public class SmartphonesListFragment extends ListFragment {
    static interface SmartphoneListener {
        void itemClicked( long id );
    }
    private SmartphoneListener l;
    public SmartphonesListFragment() {}

    @Override
    public View onCreateView (LayoutInflater inflater,
                              ViewGroup container,
                              Bundle savedInstanceState) {
        String[] smartphonenames = new String[Smartphone.phones.length];
        for (int i = 0; i <  Smartphone.phones.length; i++) {
            smartphonenames[i] = Smartphone.phones[i].getModel();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_list_item_1,smartphonenames);
        setListAdapter(adapter);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context){
        super.onAttach(context);

        if (context instanceof  Activity){
            Activity activity = (Activity) context;
            l = (SmartphoneListener) activity;
        }
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id){
        if (l != null){
            l.itemClicked(id);
        }
    }
}
