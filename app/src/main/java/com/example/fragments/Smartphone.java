package com.example.fragments;

public class Smartphone {
    private String brand;
    private String model;
    private int ram;
    private int rom;
    private String cpu;
    private int image;

    public static final Smartphone[] phones = {
            new Smartphone("Xiaomi", "Mi 9t", 6, 128, "Snapdragon 730", R.drawable.mi9t),
            new Smartphone("Apple", "iPhone 11", 4, 128, "Apple A13 Bionic", R.drawable.iphone11),
            new Smartphone("Samsung", "Galaxy S20 Ultra", 6, 128, "Exynos 990", R.drawable.galaxys20ultra)
    };

    public Smartphone(String brand, String model, int ram, int rom, String cpu, int image){
        this.brand = brand;
        this.model = model;
        this.ram = ram;
        this.rom = rom;
        this.cpu = cpu;
        this.image = image;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getRam() {
        return ram;
    }

    public int getRom() {
        return rom;
    }

    public String getCpu() {
        return cpu;
    }

    public int getImage() {
        return image;
    }
}
