package com.example.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import org.w3c.dom.Text;

public class SmartphoneInfoFragment extends Fragment {
    long smartphone_id;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState){
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.smartphoneinfo_fragment,
                container,
                false);
    }

    public void setID(long i) {
        this.smartphone_id = i;
    }

    @Override
    public void onStart(){
        super.onStart();
        View view = getView();
        if (view != null){
            TextView brandName = view.findViewById(R.id.brand);
            TextView modelName = view.findViewById(R.id.model);
            TextView memory = view.findViewById(R.id.memory);
            TextView cpu = view.findViewById(R.id.cpu);
            ImageView image = view.findViewById(R.id.image);

            Smartphone s = Smartphone.phones[(int) smartphone_id];

            brandName.setText(s.getBrand());
            modelName.setText(s.getModel());
            memory.setText(s.getRam() + "Gb " + s.getRom() + "Gb");
            image.setImageResource(s.getImage());
            cpu.setText("CPU: " + s.getCpu());
        }
    }
}
