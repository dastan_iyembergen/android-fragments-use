package com.example.fragments;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity implements SmartphonesListFragment.SmartphoneListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void itemClicked(long id) {
        SmartphoneInfoFragment f = new SmartphoneInfoFragment();
        f.setID(id);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frag_position, f)
                .commit();
    }
}
